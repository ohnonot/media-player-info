### Plugins

The script uses player-specific functions to fill an array with information.  
Builtin are mpd, audacious and mocp.  
Beyond that, one can write a function for another player and put it in a file
in this folder.

Rules to build a new plugin:

    player plugins work like this:
    - $player is the name of the player executable so that "pgrep -x $player" will tell us whether
      that player is running.
    - any file inside the plugins directory called exactly ${player}.mpip will be sourced
      automatically (unless the user specified) by the main script
    - it contains at least one function named $player_fillarray
    - the global associative array info[] is defined in the main script.
    - the function fills it with fields named ${player}0 - ${player}8 - example: info[audacious0]=...
   
      like this:
    - 0  the player's state play/pause/stop/unknown as defined in the state_nice array in the main script
    - 1  the title of the currently playing song, usually 'Song Name - Artist' or, failing that, file name
    - 2  the file (usually full path)
    - 3  the time position in the song as elapsed/total, or merely elapsed for streams
    - 4  player's play/pause toggle command
    - 5  player's next song command
    - 6  player's previous song command
    - 7  player's show window command
    - 8  icon name (as defined in global "icons" array) for the current state
   
    this array will later be filled with 2 more fields, but this is not player specific,
    hence it is not part of the function.
    see help text for -d option.
   
    if the function is called with "dryrun" as $1, it will not query the player
    but fill the array with all static values available, i.e. fields 4-7.
   
    these helper functions can be utilized:
       title_concat (formats the title to artist - songtitle, except when we listen to a
                     network stream, because those usually already format the songtitle
                     as artist - songtitle)
       filename_as_title (if no title, use filename instead)
       time_concat (concatenates the fields for time elapsed/remaining and total time
   
    the internal player functions and helper functions use a trick: the array is first
    filled with: artist,songtitle,file,currenttime,totaltime,playerstate
    all in separate fields, then the title_concat and time_concat functions reformat what
    is to become the final title and time field, after which the artist (field 0) is
    overwritten with the player's beautified state. now the last two array fields are
    useless and are overwritten with other information.
All this is fairly flexible; one could choose to display different information, it only needs to fit the `info` array.  
A simple example would be to fill the 3rd field with remaining time, and not elapsed, or use a different song title format.  
A plugin function with a name identical to a builtin will override the builtin." >> "plugins/README.md"
